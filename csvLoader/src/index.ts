import { readFile, createReadStream, fstat, existsSync } from "fs";
import csv from "csv-parser";
import axios from "axios";
import FileConstants from "./constants";
import { response } from "express";


const result: JSON[] = [];

const path = process.argv.slice(2);
const object = process.argv.slice(3);

const config = {
    headers: {
        "Content-type": "application/json",
        "Authorization": `${FileConstants.AUTH_TOKEN}`,
    },
};



const processCSV = async (filePath: string) => {
    createReadStream(filePath).
        pipe(csv()).
        on('data', (data) => result.push(data)).
        on('end', async () => {
            console.log(result);
            checkAndUploadObjects(result)
        });

}

const checkAndUploadObjects = async (result: JSON[]) => {
    console.log("processing list")

    console.log(result);
    let recordPresent = false;
    const urltoHit = `${FileConstants.url}/${object}`
    for (var i in result) {
        const jsonObj = JSON.parse(JSON.stringify(result[i]));
        console.log(jsonObj);
        // console.log('trying to hit the url - ' + urltoHit)
        var resp
        try {
            //handling search criterion for objects. Product on sku, category and brand on name. Enhance as needed
            if (object[0] === 'products') {
                //convert the price fields to number.
                if(jsonObj.hasOwnProperty("wholesalePrice") && jsonObj["wholesalePrice"]!==null && !isNaN(jsonObj["wholesalePrice"]) ){
                    jsonObj["wholesalePrice"] = + jsonObj["wholesalePrice"]
                    console.log('converted wholesale')
                }
                if(jsonObj.hasOwnProperty("retailPrice") && jsonObj["retailPrice"]!==null && !isNaN(jsonObj["retailPrice"]) ){
                    jsonObj["retailPrice"] = + jsonObj["retailPrice"]
                }
                if(jsonObj.hasOwnProperty("commission") && jsonObj["commission"]!==null && !isNaN(jsonObj["commission"]) ){
                    jsonObj["commission"] = + jsonObj["commission"]
                }

                //hit the url
                resp = await axios.get(`${urltoHit}?sku=${jsonObj.sku}`, config);
            } else {
                resp = await axios.get(`${urltoHit}?name=${jsonObj.name}`, config);
            }
            // console.log(resp.data)
            if (resp.data.size > 0) {
                recordPresent = true;
            }
        } catch (err) {
            console.log("Error observed...")
            console.log(err);
        }

        if (!recordPresent) {
            console.log('Record not found. Inserting...')

            //insert record
            try {
                resp = await axios.post(`${urltoHit}`, jsonObj, config)
            } catch (err) {
                console.log(err.response.data);
                break;
            }
            console.log("inserted");
        } else {
            console.log('Record exists. Skipping insert...')
        }
    }
}

console.log("checking for file:" + path + " for object " + object);
console.log
if (existsSync(path[0])) {
    console.log('File exists');
    processCSV(path[0]);

} else {
    console.log('File not found');
}
